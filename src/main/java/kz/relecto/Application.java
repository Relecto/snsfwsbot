package kz.relecto;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import kz.relecto.database.DBService;
import kz.relecto.database.H2Service;
import kz.relecto.posts.PostManager;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.sql.SQLException;

public class Application {
    public static void main(String[] args) {
        ApiContextInitializer.init();

        Config conf = ConfigFactory.load();

        TelegramBotsApi botsApi = new TelegramBotsApi();

        DBService dbService = new H2Service();
        try {
            dbService.init();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Bot bot = new Bot(conf.getString("bot.token"), conf.getString("bot.database"), dbService);

        ScheduledSenderThread sst = new ScheduledSenderThread(bot);
        sst.start();

        try {
            botsApi.registerBot(bot);
        } catch (TelegramApiException e) {
            e.printStackTrace();
            System.exit(101);
        }
    }
}
