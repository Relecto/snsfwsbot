package kz.relecto.patterns;

public enum Notification {
    TITLE_CHANGED,
    LOW_ON_POSTS,
    OUT_OF_POSTS;

    private String message;

    private Object change;

    public Object getChange() {
        return change;
    }

    public Notification setChange(Object change) {
        this.change = change;
        return this;
    }

    public Notification setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getMessage() {
        return message;
    }
}
