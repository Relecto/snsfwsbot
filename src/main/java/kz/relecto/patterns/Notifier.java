package kz.relecto.patterns;

public interface Notifier {
    void addReceiver(Receiver o);
    void removeReceiver(Receiver o);
    void notifyReceivers(Notification notification);
}
