package kz.relecto.patterns;

public interface Receiver {
    void update(Notification notification);
}
