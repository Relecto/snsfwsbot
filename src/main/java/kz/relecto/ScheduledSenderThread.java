package kz.relecto;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class ScheduledSenderThread extends Thread {

    private final Bot bot;
    private Config config;


    public ScheduledSenderThread(Bot bot) {
        super();
        this.bot = bot;
    }

    @Override
    public void run() {
        Timer timer;
        TimerTask tt;
        config = ConfigFactory.load();

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, config.getInt("initial.hour")); //Initial time is the time when first photo should be sent
        calendar.set(Calendar.MINUTE, config.getInt("initial.minute"));
        calendar.set(Calendar.SECOND, 0);

        while(true) {
            timer = new Timer();
            tt = new SchedulerTask(bot, this);

            setDate(calendar);

            Date triggerTime = calendar.getTime();

            System.out.println(Calendar.getInstance().getTime().toString());
            System.out.println(triggerTime.toString());
            bot.report("next photo is scheduled for " + triggerTime.toString());

            timer.schedule(tt, triggerTime);

            synchronized (this) {
                try {
                    this.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    break;
                }
            }
            System.out.println("resuming");
        }
    }

    //This sets Calendar to a time when photo should be sent
    private void setDate(Calendar calendar){
        while(Calendar.getInstance().after(calendar)){ //If time is in the past, add delay until it's in the future
            calendar.add(Calendar.HOUR_OF_DAY, config.getInt("sched.freq"));
        }
    }
}
