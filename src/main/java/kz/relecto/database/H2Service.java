package kz.relecto.database;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import kz.relecto.posts.Title;
import kz.relecto.posts.Post;
import kz.relecto.posts.PostType;
import org.h2.tools.Server;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class H2Service implements DBService {
    private Connection connection;
    private Statement defaultStatement;

    public synchronized void init() throws ClassNotFoundException, SQLException {
        Config config = ConfigFactory.load();

        Class.forName(config.getString("db.class"));
        connection = DriverManager.getConnection(config.getString("db.url"), config.getString("db.username"), config.getString("db.password"));

        defaultStatement = connection.createStatement();
        defaultStatement.execute("CREATE TABLE IF NOT EXISTS photos (" +
                "id BIGINT AUTO_INCREMENT," +
                "tg_id VARCHAR(255)," +
                "post_type VARCHAR(255)," +
                "title_id BIGINT," +
                "used BOOLEAN DEFAULT FALSE)"); //Table for storing IDs of photos in queue

        defaultStatement.execute("CREATE TABLE IF NOT EXISTS titles (" +
                "id BIGINT AUTO_INCREMENT," +
                "name VARCHAR(255))"); //Table for storing all the titles
        defaultStatement.execute("MERGE INTO titles KEY(id) VALUES(0, '" + config.getString("title.undefined")+ "')");

        Server server = Server.createTcpServer("-tcpAllowOthers", "-web").start();
        System.out.println(server.getPort());
    }

    //Following method adds photo to DB and returns ID of added post
    public synchronized Post addPhoto(String tgId, Title title, PostType postType) throws SQLException {
        String sql = "INSERT INTO photos(tg_id, post_type, title_id) VALUES ('" + tgId + "', '" + postType.name() + "', " + title.getId() + ")";
        PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.executeUpdate();

        ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
        if (generatedKeys.next()) {
            return Post.builder()
                    .setId(generatedKeys.getLong(1))
                    .setType(postType)
                    .setTitle(title)
                    .setTgId(tgId)
                    .build();
        } else {
            throw new SQLException("unable to create post");
        }
    }

    /*public synchronized String getLastPhoto() throws SQLException {
        ResultSet resultSet;
        resultSet = defaultStatement.executeQuery("SELECT * FROM photos WHERE id=max(id)");

        if (resultSet.next()) {
            return resultSet.getString("TG_ID");
        } else {
            throw new SQLException("no data is available");
        }
    }*/

    public synchronized Post getNextPhoto() throws SQLException { //If silent is true, then update won't be completed
        ResultSet resultSet; //TODO optimise this
        Statement localStatement = connection.createStatement();
        resultSet = localStatement.executeQuery("SELECT * FROM photos WHERE id=(SELECT min(id) FROM photos WHERE used=FALSE)");

        if (resultSet.next()) {
            return Post.builder()
                    .setId(resultSet.getLong("id"))
                    .setTgId(resultSet.getString("tg_id"))
                    .setTitle(getTitleById(resultSet.getLong("title_id")))
                    .setType(PostType.parseType(resultSet.getString("post_type")))
                    .build();
        } else {
            throw new SQLException("no data is available");
        }
    }

    public void markPosted(Post post) throws SQLException {
        String sql = "UPDATE photos SET used=true WHERE id=" + post.getId();
        defaultStatement.executeUpdate(sql);
    }

    @Override
    public int getQueueLength(Title title) throws SQLException {
        String sql = "SELECT COUNT(*) AS total FROM photos WHERE title_id=" + title.getId() + " AND used = false";
        ResultSet resultSet = defaultStatement.executeQuery(sql);

        if (resultSet.next()) {
            return resultSet.getInt("total");
        } else {
            throw new SQLException("no data is available");
        }
    }

    public synchronized Post getById(long id) throws SQLException { //TODO Refactor this kostyl
        String sql = "SELECT * FROM photos WHERE ID=" + id;
        Statement localStatement = connection.createStatement();
        ResultSet resultSet = localStatement.executeQuery(sql);

        if (resultSet.next()) {
            return Post.builder()
                    .setId(resultSet.getLong("id"))
                    .setTgId(resultSet.getString("tg_id"))
                    .setTitle(getTitleById(resultSet.getLong("title_id")))
                    .setType(PostType.parseType(resultSet.getString("post_type")))
                    .build();
        } else {
            throw new SQLException("no data is available");
        }
    }

    public Title addTitle(String name) throws SQLException {
        String sql = "INSERT INTO titles(name) VALUES ('" + name + "')";

        PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.executeUpdate();

        ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
        if (generatedKeys.next()) {
            return new Title(generatedKeys.getLong(1), name);
        } else {
            throw new SQLException("unable to create post");
        }

    }

    @Override
    public Title getTitle(String name) throws SQLException {
        String sql = "SELECT * FROM titles WHERE name='" + name + "'";
        ResultSet resultSet = defaultStatement.executeQuery(sql);

        if (resultSet.next()) {
            return new Title(resultSet.getInt("id"), resultSet.getString("name"));
        } else {
            throw new SQLException("no data is available");
        }
    }

    @Override
    public Title getTitle(long id) throws SQLException {
        String sql = "SELECT * FROM titles WHERE id=" + id;
        ResultSet resultSet = defaultStatement.executeQuery(sql);

        if (resultSet.next()) {
            return new Title(resultSet.getInt("id"), resultSet.getString("name"));
        } else {
            throw new SQLException("no data is available");
        }
    }

    @Override
    public List<Title> getTitles() throws SQLException {
        String sql = "SELECT * FROM titles";
        ResultSet resultSet = defaultStatement.executeQuery(sql);
        ArrayList<Title> titles = new ArrayList<>();

        while (resultSet.next()) {
            titles.add(new Title(resultSet.getInt("id"), resultSet.getString("name")));
        }

        return titles;
    }

    public Title getTitleById(long id) throws SQLException {
        ResultSet resultSet = defaultStatement.executeQuery("SELECT * FROM titles WHERE ID = " + id);

        if (resultSet.next()) {
            return new Title(resultSet.getLong("id"), resultSet.getString("name"));
        } else {
            throw new SQLException("no data is available");
        }
    }

    @Deprecated
    public synchronized void disconnect() throws SQLException {
        defaultStatement.close();
        connection.close();
    }

    public synchronized void close() throws SQLException {
        defaultStatement.close();
        connection.close();
    }
}
