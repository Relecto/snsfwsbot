package kz.relecto.database;

import kz.relecto.posts.Title;
import kz.relecto.posts.Post;
import kz.relecto.posts.PostType;

import java.sql.SQLException;
import java.util.List;

public interface DBService {
    void init() throws ClassNotFoundException, SQLException;
    void close() throws SQLException;
    Post addPhoto(String tgId, Title title, PostType postType) throws SQLException;
    Post getNextPhoto() throws SQLException;
    Post getById(long id) throws SQLException;
    Title addTitle(String name) throws SQLException;
    Title getTitle(String name) throws SQLException;
    Title getTitle(long id) throws SQLException;
    List<Title> getTitles() throws SQLException;
    void markPosted(Post post) throws SQLException;
    int getQueueLength(Title title)throws SQLException;
}
