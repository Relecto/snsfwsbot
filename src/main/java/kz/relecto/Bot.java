package kz.relecto;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import kz.relecto.database.DBService;
import kz.relecto.database.DataException;
import kz.relecto.patterns.Notification;
import kz.relecto.patterns.Receiver;
import kz.relecto.posts.Post;
import kz.relecto.posts.PostManager;
import kz.relecto.posts.PostType;
import kz.relecto.posts.Title;
import org.telegram.abilitybots.api.bot.AbilityBot;
import org.telegram.abilitybots.api.objects.Ability;
import org.telegram.abilitybots.api.objects.EndUser;
import org.telegram.abilitybots.api.objects.MessageContext;
import org.telegram.telegrambots.api.methods.send.SendDocument;
import org.telegram.telegrambots.api.methods.send.SendMediaGroup;
import org.telegram.telegrambots.api.methods.send.SendPhoto;
import org.telegram.telegrambots.api.methods.send.SendVideo;
import org.telegram.telegrambots.api.objects.Document;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.PhotoSize;
import org.telegram.telegrambots.api.objects.Video;
import org.telegram.telegrambots.api.objects.media.InputMedia;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.ToIntFunction;

import static org.telegram.abilitybots.api.objects.Locality.ALL;
import static org.telegram.abilitybots.api.objects.Locality.USER;
import static org.telegram.abilitybots.api.objects.Privacy.ADMIN;
import static org.telegram.abilitybots.api.objects.Privacy.PUBLIC;

public class Bot extends AbilityBot implements Receiver {

    private final PostManager postManager;
    private final Config config;
    private boolean silence = false;
    private static Map<String, String> settings;

    protected Bot(String botToken, String botUsername, DBService dbService) {/**/
        super(botToken, botUsername);

        config = ConfigFactory.load();
        settings = db.getMap("Settings");

        this.postManager = new PostManager(dbService);

        postManager.addReceiver(this);
        report("init complete.\n" +
                "version: " + config.getString("bot.version") + "\n" +
                "local time: " + Calendar.getInstance().getTime().toString());
        toggleSilence();
    }

    public int creatorId() {
        return config.getInt("bot.creator");
    }

    public Long mainChatId() {
        return config.getLong("bot.mainchat");
    }

    public Ability sayHello() {
        return Ability.builder()
                .name("hello")
                .info("says hello")
                .locality(ALL)
                .privacy(PUBLIC)
                .action(ctx -> silent.send("hi there", ctx.chatId()))
                .build();
    }

    public Ability start() {
        return Ability.builder()
                .name("start")
                .locality(ALL)
                .privacy(PUBLIC)
                .action(ctx -> silent.send("nice to meet you, " + ctx.user().firstName(), ctx.chatId()))
                .build();
    }

    public Ability subscribe() {
        return Ability.builder()
                .name("subscribe")
                .locality(ALL)
                .privacy(ADMIN)
                .action(ctx -> {
                    List<EndUser> users = db.getList("NotifyList");
                    if (users.contains(ctx.user())) {
                        silent.send("you are already subscribed 🙃", ctx.chatId());
                    } else {
                        users.add(ctx.user());
                        silent.send("cool, you are a subscriber now 🌚", ctx.chatId());
                    }
                })
                .build();
    }

    public Ability unsubscribe() {
        return Ability.builder()
                .name("unsubscribe")
                .locality(ALL)
                .privacy(ADMIN)
                .action(ctx -> {
                    List<EndUser> users = db.getList("NotifyList");
                    if (!users.contains(ctx.user())) {
                        silent.send("have you ever tried subscribing? 🤔", ctx.chatId());
                    } else {
                        users.remove(ctx.user());
                        silent.send("i will not notify you anymore 😔", ctx.chatId());
                    }
                })
                .build();
    }

    public Ability enableAnnounce() {
        return Ability.builder()
                .name("announceon")
                .locality(ALL)
                .privacy(ADMIN)
                .action(ctx -> {
                    set("announce", "true");
                    silent.send("🔔 announcements enabled", ctx.chatId());
                })
                .build();
    }

    public Ability disableAnnounce() {
        return Ability.builder()
                .name("announceoff")
                .locality(ALL)
                .privacy(ADMIN)
                .action(ctx -> {
                    set("announce", "false");
                    silent.send("🔕 announcements disabled", ctx.chatId());
                })
                .build();
    }

    public Ability savePost() {
        return Ability.builder()
                .name(DEFAULT)
                .privacy(ADMIN)
                .locality(USER)
                .input(0)
                .action(ctx -> {
                    Post post;
                    String tgId;
                    PostType postType;
                    Title title = postManager.getUpload();

                    //detecting and setting TgId and PostType
                    if (msg(ctx).hasPhoto()) {
                        List<PhotoSize> photo = msg(ctx).getPhoto();

                        tgId = photo.get(photo.size() - 1).getFileId();
                        postType = PostType.PHOTO;
                    } else if (checkVideo(msg(ctx))) {
                        Video video = msg(ctx).getVideo();

                        tgId = video.getFileId();
                        postType = PostType.VIDEO;
                    } else if (msg(ctx).hasDocument()) {
                        Document document = msg(ctx).getDocument();

                        tgId = document.getFileId();
                        postType = PostType.DOCUMENT;
                    } else {
                        silent.send("failed to compose post.", ctx.chatId());
                        return;
                    }

                    //assigning title
                    //looking for title id or name in message
                    if (msg(ctx).hasText()) { //check if message contains text
                        title = postManager.parseTitle(msg(ctx).getText()); //try to find given title

                        if (title == null) { //if null was returned, create a new title
                            try {
                                title = postManager.addTitle(msg(ctx).getText());
                            } catch (DataException e) {
                                silent.send("unable to create new title", ctx.chatId());
                            }
                        }
                    } else if (msg(ctx).getCaption() != null) { //check if message contains caption
                        title = postManager.parseTitle(msg(ctx).getCaption());

                        if (title == null) {
                            try {
                                title = postManager.addTitle(msg(ctx).getCaption());
                            } catch (DataException e) {
                                silent.send("unable to create new title", ctx.chatId());
                            }
                        }
                    }

                    try {
                        post = postManager.addPost(tgId, postType, title);
                    } catch (DataException e) {
                        silent.send("unable to create new post", ctx.chatId());
                        return;
                    }

                    silent.send("success. #" + post.getId() + "\n" +
                            "title: " + post.getTitle().toString(), ctx.chatId());
                }).build();
    }

    public Ability getUpload() {
        return Ability.builder()
                .name("upload")
                .privacy(ADMIN)
                .locality(ALL)
                .input(0)
                .action(ctx -> {
                    silent.send(postManager.getUpload().toString(), ctx.chatId());
                }).build();
    }

    public Ability setUpload(){
        return Ability.builder()
                .name("setupload")
                .privacy(ADMIN)
                .locality(ALL)
                .input(1)
                .action(ctx -> {
                    postManager.setUpload(postManager.parseTitle(ctx.firstArg()));
                    silent.send("upload title is successfully set to " + postManager.getUpload().getName(), ctx.chatId());
                }).build();
    }

    public Ability nextPhoto() {
        return Ability.builder()
                .name("next")
                .privacy(ADMIN)
                .locality(ALL)
                .input(0)
                .action(ctx -> {
                    try {
                        Post post = postManager.get();
                        sendPost(post, ctx.chatId());
                        silent.send("#" + post.getId() + "\n"
                                + "title: " + post.getTitle().toString(), ctx.chatId());
                    } catch (TelegramApiException e) {
                        silent.send("unable to find next photo:\n" + e.getMessage(), ctx.chatId());
                    } catch (DataException e) {
                        silent.send("unable to find next photo:\n" + e.getMessage(), ctx.chatId());
                    }
                }).build();
    }

    public Ability getPhotoById() {
        return Ability.builder()
                .name("id")
                .privacy(ADMIN)
                .locality(ALL)
                .input(1)
                .action(ctx -> {
                    try {
                        Post post = postManager.get(Integer.parseInt(ctx.firstArg()));
                        sendPost(post, ctx.chatId());
                        silent.send("#" + post.getId() + "\n"
                                + "title: " + post.getTitle().toString(), ctx.chatId());
                    } catch (TelegramApiException e) {
                        silent.send("error sending message", ctx.chatId());
                    } catch (DataException e) {
                        e.printStackTrace();
                        silent.send("no data is available", ctx.chatId());
                    }
                }).build();
    }

    public Ability currentTitle() {
        return Ability.builder()
                .name("current")
                .privacy(ADMIN)
                .locality(ALL)
                .input(0)
                .action(ctx -> {
                    silent.send("#" + postManager.getTitle().getId() + "\n"
                            + postManager.getTitle().getName(), ctx.chatId());
                }).build();
    }

    public Ability title() {
        return Ability.builder()
                .name("title")
                .privacy(ADMIN)
                .locality(ALL)
                .input(1)
                .action(ctx -> {
                    Title title = postManager.parseTitle(ctx.firstArg());
                    if (title != null) {
                        silent.send(title.toString() +  " (" + postManager.queueLength(title) + ")", ctx.chatId());
                    } else {
                        silent.send("cannot find given title", ctx.chatId());
                    }
                }).build();
    }

    public Ability titles() {
        return Ability.builder()
                .name("titles")
                .privacy(ADMIN)
                .locality(ALL)
                .input(0)
                .action(ctx -> {
                    List<Title> titles;
                    StringBuilder output = new StringBuilder("available titles:\n");

                    //retrieve title list
                    try {
                        titles = postManager.titles();
                    } catch (DataException e) {
                        silent.send("no titles are available", ctx.chatId());
                        return;
                    }

                    //sort titles by their queueLength
                    titles.sort(Comparator.comparingInt((Title t) -> postManager.queueLength(t)).reversed());

                    for (Title title : titles) {
                        output.append("#").append(title.getId()).append(" ")
                                .append(title.getName()).append(" (")
                                .append(postManager.queueLength(title)).append(")\n");
                    }

                    silent.send(output.toString(), ctx.chatId());
                }).build();
    }

    public Ability setTitle() {
        return Ability.builder()
                .name("settitle")
                .privacy(ADMIN)
                .locality(ALL)
                .input(1)
                .action(ctx -> {
                    postManager.setTitle(postManager.parseTitle(ctx.firstArg()));
                    silent.send("title is successfully set to " + postManager.getTitle().getName(), ctx.chatId());
                }).build();
    }

    public Ability postNow() {
        return Ability.builder()
                .name("post")
                .privacy(ADMIN)
                .locality(ALL)
                .input(0)
                .action(ctx -> post()).build();
    }

    public Ability silenceAbility() {
        return Ability.builder()
                .name("silence")
                .privacy(ADMIN)
                .locality(ALL)
                .input(0)
                .action(ctx -> {
                    toggleSilence();
                    if (silence) {
                        silent.send("silent.", ctx.chatId());
                    } else {
                        silent.send("very loud.", ctx.chatId());
                    }
                }).build();
    }

    public Ability version() {
        return Ability.builder()
                .name("version")
                .privacy(ADMIN)
                .locality(USER)
                .input(0)
                .action(ctx -> silent.send(config.getString("bot.version"), ctx.chatId()))
                .build();
    }

    public Ability dev() {
        return Ability.builder()
                .name("do")
                .locality(USER)
                .privacy(ADMIN)
                .input(1)
                .action(ctx -> {

                    if (ctx.firstArg().equals("not")) { //APACHA
                        silent.send("DB connection closed, terminating", ctx.chatId());
                        System.exit(1);
                    } else {
                        silent.send("do what?", ctx.chatId());
                    }

                })
                .build();
    }

    private Message msg(MessageContext ctx) {
        return ctx.update().getMessage();
    }

    //checks if message contains video using Reflection API
    private boolean checkVideo(Message message) {
        try {
            Field videoField = message.getClass().getDeclaredField("video");
            videoField.setAccessible(true);

            Video video = (Video) videoField.get(message);

            if (video != null) {
                videoField.setAccessible(false);
                return true;
            } else {
                videoField.setAccessible(false);
                return false;
            }
        } catch (NoSuchFieldException | IllegalAccessException e) {
            return false;
        }


    }

    public void toggleSilence() {
        silence = !silence;
    }

    public void post() {
        Post post = null;
        try {
            post = postManager.get();
        } catch (DataException e) {
            //report(e);
            return;
        }

        try {
            sendPost(post, mainChatId());
        } catch (TelegramApiException e) {
            //report(e);
            //TODO: Add scheduler task: repost
            return;
        }

        try {
            postManager.markPosted(post);
        } catch (DataException e) {
            report(e);
        }
    }

    public void sendPost(Post post, long chatId) throws TelegramApiException {
        if (post.getType() == PostType.PHOTO) {
            SendPhoto sendPhoto = new SendPhoto();
            sendPhoto.setPhoto(post.getTgId());
            sendPhoto.setChatId(chatId);

            sender.sendPhoto(sendPhoto);

        } else if (post.getType() == PostType.VIDEO) {
            SendVideo sendVideo = new SendVideo();
            sendVideo.setVideo(post.getTgId());
            sendVideo.setChatId(chatId);

            sender.sendVideo(sendVideo);
        } else if (post.getType() == PostType.DOCUMENT) {
            SendDocument sendDocument = new SendDocument();
            sendDocument.setDocument(post.getTgId());
            sendDocument.setChatId(chatId);

            sender.sendDocument(sendDocument);
        }
    }

    public void sendMediaGroup(List<InputMedia> inputMedia, long chatId) {
        SendMediaGroup sendMediaGroup = new SendMediaGroup();
        sendMediaGroup.setMedia(inputMedia);
        sendMediaGroup.setChatId(chatId);

    }

    public void report(String message) {
        if (!silence) {
            silent.send(message, creatorId());
        }
    }

    public void report(Exception e) {
        silent.send(e.getMessage(), creatorId());
    }

    public void notifyList(String message) {
        List<EndUser> users = db.getList("NotifyList");
        for (EndUser user : users) {
            silent.send(message, user.id());
        }
    }

    @Override
    public void update(Notification notification) {
        if (notification == Notification.LOW_ON_POSTS) {
            notifyList(notification.getMessage());
        } else if (notification == Notification.TITLE_CHANGED) {
            notifyList(notification.getMessage());
            if (Boolean.valueOf(get("announce"))) {
                silent.send(config.getString("notify.announcement")
                        .replaceAll("\\$title", postManager.getTitle().getName()), mainChatId());
            }
        } else if (notification == Notification.OUT_OF_POSTS) {
            notifyList("out of posts for current title. Performing auto title change");

            List<Title> titles;
            try {
                titles = postManager.titles();
            } catch (DataException e) {
                notifyList("unable to change title automatically, please perform manual change");
                return;
            }
            if (titles != null) {
                titles.sort(Comparator.comparingInt((ToIntFunction<Title>) postManager::queueLength).reversed());
            } else {
                notifyList("unable to change title automatically, please perform manual change");
                return;
            }

            if (postManager.queueLength(titles.get(0)) == 0) {
                notifyList("unable to change title automatically, please perform manual change");
                return;
            }

            postManager.setTitle(titles.get(0));
        }
    }

    public static void set(String key, String value) {
        settings.put(key, value);
    }

    public static String get(String key) {
        return settings.get(key);
    }
}