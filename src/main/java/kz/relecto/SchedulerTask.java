package kz.relecto;

import java.util.TimerTask;

public class SchedulerTask extends TimerTask {

    private final Bot bot;
    private final Thread invoker;

    public SchedulerTask(Bot bot, Thread invoker){
        this.bot = bot;
        this.invoker = invoker;
    }
    @Override
    public void run() {
        bot.post();
        synchronized (invoker) {
            invoker.notify();
        }
    }
}