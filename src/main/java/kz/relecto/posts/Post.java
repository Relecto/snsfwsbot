package kz.relecto.posts;

public class Post {
    private final long id;
    private final String tgId;
    private final Title title;
    private final PostType type;

    public Post(long id, String tgId, Title title, PostType type) {
        this.id = id;
        this.tgId = tgId;
        this.title = title;
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public String getTgId() {
        return tgId;
    }

    public Title getTitle() {
        return title;
    }

    public PostType getType() {
        return type;
    }

    public static PostBuilder builder() {
        return new PostBuilder();
    }

    public static class PostBuilder {
        private long id;
        private String tgId;
        private Title title;
        private PostType type;

        private PostBuilder(){

        }

        public PostBuilder setId(long id) {
            this.id = id;
            return this;
        }

        public PostBuilder setTgId(String tgId) {
            this.tgId = tgId;
            return this;
        }

        public PostBuilder setTitle(Title title) {
            this.title = title;
            return this;
        }

        public PostBuilder setType(PostType type) {
            this.type = type;
            return this;
        }

        public Post build() {
            return new Post(id, tgId, title, type);
        }
    }
}
