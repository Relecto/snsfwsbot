package kz.relecto.posts;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public class Title {
    private long id;
    private String name;

    public final static Title UNDEFINED;

    static {
        Config config = ConfigFactory.load();
        UNDEFINED = new Title(0, config.getString("title.undefined"));
    }

    public Title(long id, String name){
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public long getId() {
        return id;
    }

    @Override
    public String toString(){
        return "#" + id + " " + name;
    }
}
