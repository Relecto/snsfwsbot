package kz.relecto.posts;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import kz.relecto.Bot;
import kz.relecto.database.DBService;
import kz.relecto.database.DataException;
import kz.relecto.patterns.Notification;
import kz.relecto.patterns.Notifier;
import kz.relecto.patterns.Receiver;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PostManager implements Notifier {
    private Title current;
    private Title upload;
    private final DBService dbService;
    private Config config;

    private ArrayList<Receiver> receivers;

    public PostManager(DBService dbService) {
        receivers = new ArrayList<>();
        this.dbService = dbService;
        config = ConfigFactory.load();

        try {
            String title = Bot.get("title");
            if (title != null) {
                current = getTitle(title);
            } else {
                current = null;
            }
        } catch (DataException e) {
            current = null;
        }

        if (current == null) {
            current = Title.UNDEFINED;
        }

        upload = Title.UNDEFINED;
    }

    public Post get() throws DataException {
        try {
            return dbService.getNextPhoto();
        } catch (SQLException e) {
            throw new DataException(e.getMessage());
        }
    }

    public Post get(long id) throws DataException {
        try {
            return dbService.getById(id);
        } catch (SQLException e) {
            throw new DataException(e.getMessage());
        }
    }

    public int queueLength() {
        try {
            return dbService.getQueueLength(current);
        } catch (SQLException e) {
            return 0;
        }
    }

    public int queueLength(Title title) {
        try {
            return dbService.getQueueLength(title);
        } catch (SQLException e) {
            return 0;
        }
    }

    public void markPosted(Post post) throws DataException {
        try {
            dbService.markPosted(post);
        } catch (SQLException e) {
            throw new DataException(e.getMessage());
        }

        if (queueLength() == config.getInt("notify.threshold")) {
            Notification notification = Notification.LOW_ON_POSTS;
            notification.setMessage(queueLength() + " posts left in the queue for " + current.toString());
            notification.setChange(current);
            notifyReceivers(notification);
        } else if (queueLength() == 0) {
            Notification notification = Notification.OUT_OF_POSTS;
            notification.setMessage("No posts left for " + current.toString());
            notification.setChange(current);
            notifyReceivers(notification);
        }

    }

    public Post addPost(String tgId, PostType type, Title title) throws DataException {
        try {
            return dbService.addPhoto(tgId, title, type);
        } catch (SQLException e) {
            throw new DataException(e.getMessage());
        }
    }

    public Post addPost(String tgId, PostType type) throws DataException {
        try {
            return dbService.addPhoto(tgId, upload, type);
        } catch (SQLException e) {
            throw new DataException(e.getMessage());
        }
    }

    public Title getTitle() {
        return current;
    }

    public void setTitle(Title title) {
        current = title;
        Bot.set("title", title.getName());

        Notification notification = Notification.TITLE_CHANGED;
        notification.setChange(title);
        notification.setMessage("title just changed to " + title.toString());

        notifyReceivers(notification);
    }

    public void setUpload(Title title) {
        upload = title;
    }

    public Title getUpload() {
        return upload;
    }

    public Title parseTitle(String text) {
        Title title;

        if (text.chars().allMatch(Character::isDigit)) {
            try {
                title = getTitle(Long.parseLong(text));
            } catch (DataException e) {
                return null;
            }
        } else {
            try {
                title = getTitle(text);
            } catch (DataException e) {
                title = null;
            }
        }

        return title;
    }

    public Title getTitle(String name) throws DataException {
        try {
            return dbService.getTitle(name);
        } catch (SQLException e) {
            throw new DataException(e.getMessage());
        }
    }

    public Title getTitle(long id) throws DataException {
        try {
            return dbService.getTitle(id);
        } catch (SQLException e) {
            throw new DataException(e.getMessage());
        }
    }

    public List<Title> titles() throws DataException {
        try {
            return dbService.getTitles();
        } catch (SQLException e) {
            throw new DataException(e.getMessage());
        }
    }

    public Title addTitle(String name) throws DataException {
        try {
            return dbService.addTitle(name);
        } catch (SQLException e) {
            throw new DataException(e.getMessage());
        }
    }

    @Override
    public void addReceiver(Receiver o) {
        receivers.add(o);
    }

    @Override
    public void removeReceiver(Receiver o) {
        receivers.remove(o);
    }

    @Override
    public void notifyReceivers(Notification notification) {
        for (Receiver o : receivers) {
            o.update(notification);
        }
    }
}
