package kz.relecto.posts;

public enum PostType {
    PHOTO,
    VIDEO,
    DOCUMENT;

    public static PostType parseType(String type){
        if (type.equals("PHOTO")){
            return PostType.PHOTO;
        } else if (type.equals("VIDEO")){
            return PostType.VIDEO;
        } else if (type.equals("DOCUMENT")) {
            return PostType.DOCUMENT;
        } else {
            return null;
        }
    }
}
